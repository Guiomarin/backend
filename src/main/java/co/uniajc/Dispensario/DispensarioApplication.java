package co.uniajc.Dispensario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DispensarioApplication {

    public static void main(String[] args) {
    	SpringApplication.run(DispensarioApplication.class, args);
    }

}
